export const pipeline = (...functions: ReadonlyArray<(next: (value: unknown) => void) => void>) => {
    const recurseOperate = (i: number, finished: (value: unknown) => void) => {
        new Promise(functions[i])
            .finally(() => {
                if (functions.length !== i + 1) {
                    recurseOperate(i+1, finished);
                } else {
                    finished(null);
                }
            });
    }
    return new Promise((finished) => {
        recurseOperate(0, finished);
    });
}