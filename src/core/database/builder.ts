import { Model } from "mongoose";

export const builder = <ResourceT>(model: Model<unknown>) => {
    return (attr: ResourceT) => (new model(attr));
}