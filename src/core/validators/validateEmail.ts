import validator from "validator";

export const validateEmail = (email: string) => email && validator.isEmail(email);