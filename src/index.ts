import { server } from "./server";
import mongoose from "mongoose";
import dotenv from "dotenv";
import figlet from "figlet";
import {statusLog} from "./core/utils";
import {io} from "socket.io-client";

dotenv.config();

const databaseConnection = mongoose.connect(`${process.env.DATABASE_CONNECTION_STRING}?connectTimeoutMS=100`);

const port = Number(process.env.SERVER_PORT || 3000);

const socket = io(process.env.SOCKET_URL);

server({
    databaseConnection,
    socket
})
    .then(app => {
        app.listen(port, () => {
            statusLog('success', `Listener successfully launched`);
            figlet("Kinopark API", (err, data) => {
                if (err === null) {
                    console.log("_____________");
                    console.log("\x1b[32m%s\x1b[0m", data);
                    console.log(`All services started, \x1b[32mhttp://localhost:${ port }\x1b[0m`);
                }
            });
        });
    });

