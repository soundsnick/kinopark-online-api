import mongoose from "mongoose";
import { dropAllCollections } from "./database";

type SetupHooks = {
    beforeAll?: () => Promise<void>,
    afterEach?: () => Promise<void>,
    afterAll?: () => Promise<void>,
} | null;

export const setup = (suffix: string, extraHooks: SetupHooks = null) => {
    beforeAll(async () => {
        await mongoose.connect(`${process.env.TEST_DATABASE_CONNECTION_STRING}_${suffix}`);
        if (extraHooks?.beforeAll) {
            await extraHooks.beforeAll()
        }
    });

    afterEach(async () => {
        if (extraHooks?.afterEach) {
            await extraHooks.afterEach()
        }
    });

    afterAll(async () => {
        if (extraHooks?.afterAll) {
            await extraHooks.afterAll()
        }
        await dropAllCollections();
        await mongoose.connection.close();
    });
}