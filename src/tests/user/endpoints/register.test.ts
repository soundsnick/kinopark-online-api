import { getApp } from "../../server";
import { generateRoute } from "../../../core/router";
import { messages } from '../../../core';
import { registerUser } from "../mocks/registerUser";
import { existingUser } from "../mocks/existingUser";
import { setup } from "../../setup";
import { initializeExistingUser } from "../utils";

setup('user', {
    beforeAll: async () => {
        await initializeExistingUser();
    }
});

describe("POST /auth/register", () => {
    test("Wrong input case", async () => {
        expect.assertions(2);
        const request = await getApp();
        return request.post(generateRoute('/auth/register'))
            .then(res => {
                expect(res.status).toBe(400);

                const body = JSON.parse(res.text);
                expect(body?.message).toBe(messages.Errors.WRONG_INPUT);
            });
    });

    test("Successful registration case", async () => {
        expect.assertions(2);
        const request = await getApp();
        return request.post(generateRoute('/auth/register'))
            .send(registerUser)
            .then(res => {
                expect(res.status).toBe(200);

                const body = JSON.parse(res.text);
                expect(body?.session).toBeDefined();
            });
    });

    test("Try to register exist user", async () => {
        expect.assertions(2);
        const request = await getApp();
        return request.post(generateRoute('/auth/register'))
            .send(existingUser)
            .then(res => {
                expect(res.status).toBe(400);

                const body = JSON.parse(res.text);
                expect(body?.message).toBe(messages.Errors.ALREADY_EXISTS);
            });
    });
});
