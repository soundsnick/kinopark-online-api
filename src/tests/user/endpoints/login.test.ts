import { getApp } from "../../server";
import { generateRoute } from "../../../core/router";
import { messages } from '../../../core';
import { setup } from "../../setup";
import { initializeExistingUser } from "../utils";
import { existingUser } from "../mocks/existingUser";

setup('user', {
    beforeAll: async () => {
        await initializeExistingUser();
    }
});

describe("POST /auth/login", () => {
    test("Wrong input case", async () => {
        expect.assertions(2);
        const request = await getApp();
        return request.post(generateRoute('/auth/login'))
            .then(res => {
                expect(res.status).toBe(400);

                const body = JSON.parse(res.text);
                expect(body?.message).toBe(messages.Errors.WRONG_INPUT);
            });
    });

    test("Successful login case", async () => {
        expect.assertions(2);
        const request = await getApp();
        return request.post(generateRoute('/auth/login'))
            .send(existingUser)
            .then(res => {
                expect(res.status).toBe(200);

                const body = JSON.parse(res.text);
                expect(body?.token).toBeDefined();
            });
    });

    test("Wrong credentials case", async () => {
        expect.assertions(2);

        const payload = {
            email: existingUser.email,
            password: "random_password"
        };

        const request = await getApp();
        return request.post(generateRoute('/auth/login'))
            .send(payload)
            .then(res => {
                expect(res.status).toBe(400);

                const body = JSON.parse(res.text);
                expect(body?.message).toBe(messages.Errors.WRONG_CREDENTIALS);
            });
    });
});
