import { build } from "../../modules/user/model";
import { existingUser } from "./mocks/existingUser";

export const initializeExistingUser = () => {
    const newUser = build(existingUser);
    return newUser.save();
}