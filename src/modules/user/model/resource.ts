import mongoose from "mongoose";
import { Resource as SeanceResource } from "../../seance/model";
import { Resource as MessageResource } from "../../message/model";

export type Resource = {
    readonly id?: mongoose.Types.ObjectId,
    readonly email: string;
    readonly name?: string;
    readonly password: string;
    readonly seances?: SeanceResource[];
    readonly messages?: MessageResource[];
}
