import mongoose from "mongoose";

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        default: null
    },
    password: {
        type: String,
        required: true
    },
    seances: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Seance",
        default: []
    },
    messages: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Message",
        default: []
    }
});

export const model = mongoose.model('User', schema);

