import { Request, Response } from "express";
import { model as modelSession } from "../../session/model";
import { messages } from "../../../core";

export const logout = async (req: Request, res: Response) => {
    const input = req.body;
    if (input?.token) {
        modelSession.remove({ token: input.token }, (err: unknown) => {
           if (err) {
               return res.status(500).send({ message: messages.Errors.SERVER_ERROR });
           } else {
               return res.send({ message: messages.Success.Default });
           }
        });
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};