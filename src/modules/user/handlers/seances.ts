import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";

export const seances = async (req: Request, res: Response) => {
    const user = await authenticate(req);
    if (user) {
        return res.status(200).send(user.toObject().seances);
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};
