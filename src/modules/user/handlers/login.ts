import { Request, Response } from "express";
import { model } from "../model";
import { build as buildSession } from "../../session/model";
import * as _ from 'lodash';
import validator from 'validator';
import { generateToken } from "../../../core/utils";
import { messages } from "../../../core";

export const login = async (req: Request, res: Response) => {
    const input = req.body;
    if (validator.isEmail(input?.email || '') && input?.password) {
        const user = await model.findOne({ email: input.email.toLowerCase(), password: input.password }).exec();
        if (user) {
            const session = buildSession({user: user.id, token: generateToken(30)});
            session.save((errSession) => {
                if (errSession) {
                    return res.status(500).send({ message: messages.Errors.SERVER_ERROR });
                } else {
                    return res.status(200).send({
                        ..._.pick(session.toObject(), ['token'])
                    });
                }
            });
        } else {
            return res.status(400).send({ message: messages.Errors.WRONG_CREDENTIALS })
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};
