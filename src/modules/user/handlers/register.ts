import { Request, Response } from "express";
import { build } from "../model";
import { build as buildSession } from "../../session/model";
import * as _ from 'lodash';
import { generateToken } from "../../../core/utils";
import { validateEmail, validatePassword } from "../../../core/validators";
import { messages } from "../../../core";

export const register = async (req: Request, res: Response) => {
    const input = req.body;
    if (validateEmail(input?.email) && validatePassword(input?.password)) {
        // Create user
        const user =  build({ email: input.email.toLowerCase(), password: input.password, name: input.name });
        user.save((err: unknown) => {
            if (err) {
                return res.status(400).send({ message: messages.Errors.ALREADY_EXISTS });
            } else {
                // Begin session
                const session = buildSession({user: user.id, token: generateToken(30)});
                session.save((errSession) => {
                    if (errSession) {
                        return res.status(500).send({ message: messages.Errors.SERVER_ERROR });
                    } else {
                        return res.status(200).send({
                            user: _.omit(user.toObject(), ['password', '_id', '__v']),
                            session: _.pick(session.toObject(), ['token'])
                        });
                    }
                });
            }
        });
    } else {
        return res.status(400).send({
           message: messages.Errors.WRONG_INPUT
        });
    }
};
