import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/auth/login'), handlers.login);
    app.post(generateRoute('/auth/register'), handlers.register);
    app.post(generateRoute('/auth/logout'), handlers.logout);
    app.get(generateRoute('/auth/user'), handlers.userByToken);
    app.get(generateRoute('/auth/user/seances'), handlers.seances);
}
