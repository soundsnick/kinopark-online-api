import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/film'), handlers.create);
    app.delete(generateRoute('/film/:id'), handlers.remove);
    app.get(generateRoute('/films'), handlers.list);
    app.get(generateRoute('/film/:id'), handlers.get);
    app.post(generateRoute('/film/:id/ready'), handlers.ready);
    app.get(generateRoute('/film/:id/seances'), handlers.seances);
}
