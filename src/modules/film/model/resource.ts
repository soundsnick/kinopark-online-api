import mongoose from "mongoose";
import { Resource as SeanceResource } from "../../seance/model";

export type Resource = {
    readonly id?: mongoose.Types.ObjectId,
    readonly title: string;
    readonly description?: string;
    readonly image: string;
    readonly promoImage: string;
    readonly director: string;
    readonly seances?: SeanceResource[];
    readonly filmFile?: string;
    readonly ready?: boolean;
    readonly duration: number;
};
