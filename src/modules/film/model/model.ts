import mongoose from "mongoose";

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        default: null
    },
    director: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    promoImage: {
        type: String,
        required: true
    },
    seances: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Seance",
        default: []
    },
    filmFile: {
        type: String,
        default: null
    },
    ready: {
        type: mongoose.SchemaTypes.Boolean,
        default: false
    },
    duration: {
        type: "Number",
        default: 0,
    }
});

export const model = mongoose.model('Film', schema);

