import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import {checkObjectId} from "../../../core/database";
import {model} from "../model";

export const seances = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    if (user && checkObjectId(params.id)) {
        const film = await model.findById(params.id).populate({
            path: 'seances',
            populate: {
                path: 'users'
            }
        });
        if (film) {
            return res.status(200).send(film.toObject().seances);
        } else {
            return res.status(404).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};
