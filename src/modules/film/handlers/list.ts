import { Request, Response } from "express";
import {build, model} from "../model";
import { messages } from "../../../core";

export const list = async (req: Request, res: Response) => {
    const films = await model.find();
    return res.status(200).send(films);
};
