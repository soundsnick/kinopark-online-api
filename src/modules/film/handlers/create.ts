import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import {authenticate} from "../../session/services";

export const create = async (req: Request, res: Response) => {
    const input = req.body;
    const user = await authenticate(req);
    if (user) {
        if (input?.title && input?.image) {
            // Create user
            const film =  build({ title: input.title, image: input.image, description: input.description, director: input.director, promoImage: input.promoImage, duration: input.duration });
            film.save((err: unknown) => {
                if (err) {
                    return res.status(400).send({ message: messages.Errors.ALREADY_EXISTS });
                } else {
                    return res.status(200).send(film.toObject());
                }
            });
        } else {
            return res.status(400).send({
                message: messages.Errors.WRONG_INPUT
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};
