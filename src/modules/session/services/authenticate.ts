import {Request} from "express";
import {model} from "../model";
import {model as modelUser} from "../../user/model";

export const authenticate = async (req: Request) => {
    const authHeader = req.headers.authorization?.split(" ");
    if (authHeader?.length === 2) {
        const [type, token] = authHeader;
        const session = await model.findOne({ token }).exec();
        if (session?.user) {
            return await modelUser.findById(session.user).populate({ path: 'seances', populate: 'film' }).exec();
        }
        return null;
    } else {
        return null;
    }
}
