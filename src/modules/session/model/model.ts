import mongoose from "mongoose";

const schema = new mongoose.Schema({
    token: {
        type: String,
        required: true,
    },
    user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
    },
});

export const model = mongoose.model('Session', schema);

