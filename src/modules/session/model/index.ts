import { database } from '../../../core';
import {model} from "./model";
import {Resource} from "./resource";

export * from './model';
export * from './resource';
export const build = database.builder<Resource>(model);