import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";

export const routes = (app: Express) => {
    app.post(generateRoute('/seance/create/:id'), handlers.create);
    app.get(generateRoute('/seance/:id'), handlers.get);
    app.post(generateRoute('/seance/purchase/:id'), handlers.purchase);
}
