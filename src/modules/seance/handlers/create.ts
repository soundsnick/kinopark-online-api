import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import {checkObjectId} from "../../../core/database";
import { model as filmModel } from "../../film/model";

export const create = async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    if (checkObjectId(params.id)) {
        // Create user
        const film = await filmModel.findById(params.id);
        if (film) {
            const seance = build({ film: film._id, datetime: input.datetime, price: input.price });
            seance.save(async (err) => {
                if (err) {
                    return res.status(500);
                }
                await filmModel.findByIdAndUpdate(film._id, { $addToSet: { seances: seance._id } })
                return res.status(200).send(seance.toObject());
            });
        } else {
            return res.status(404).send({ message: messages.Errors.NOT_FOUND });
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};
