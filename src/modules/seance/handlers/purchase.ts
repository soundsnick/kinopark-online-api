import { Request, Response } from "express";
import { messages } from "../../../core";
import { authenticate } from "../../session/services";
import {checkObjectId} from "../../../core/database";
import {model} from "../model";
import {model as userModel} from '../../user/model';

export const purchase = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    if (user && checkObjectId(params.id)) {
        const seance = await model.findById(params.id).populate('users').exec();
        if (seance) {
            if (!seance.toObject().users.includes(user.toObject())) {
                await model.findByIdAndUpdate(params.id,{ $addToSet: { users: user._id } });
                await userModel.findByIdAndUpdate(user._id, { $addToSet: { seances: seance._id } });
                return res.status(200).send(seance);
            } else {
                return res.status(400).send({
                    message: messages.Errors.ALREADY_EXISTS
                })
            }
        } else {
            return res.status(404).send({
                message: messages.Errors.NOT_FOUND
            });
        }
    } else {
        return res.status(401).send({
            message: messages.Errors.UNAUTHORIZED
        });
    }
};
