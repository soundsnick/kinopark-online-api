import mongoose from "mongoose";

const schema = new mongoose.Schema({
    datetime: {
        type: mongoose.SchemaTypes.Date,
        required: true,
    },
    price: {
        type: "Number",
        required: true
    },
    film: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Film",
    },
    users: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "User"
    },
    messages: {
        type: [mongoose.SchemaTypes.ObjectId],
        ref: "Message"
    }
});

export const model = mongoose.model('Seance', schema);

