import { Resource as UserResource} from "../../user/model";
import { Resource as MessageResource } from "../../message/model";

export type Resource = {
    readonly datetime: Date;
    readonly price: number;
    readonly film: string;
    readonly users?: UserResource[];
    readonly messages?: MessageResource[];
}
