import { Request, Response } from "express";
import { build } from "../model";
import { messages } from "../../../core";
import {checkObjectId} from "../../../core/database";
import { model as seanceModel } from "../../seance/model";
import {authenticate} from "../../session/services";

export const list = async (req: Request, res: Response) => {
    const params = req.params;
    const user = await authenticate(req);
    if (user && checkObjectId(params.id)) {
        // Create user
        const seance = await seanceModel.findById(params.id).populate({
            path: 'messages',
            populate: ['user', 'seance']
        });
        if (seance) {
            return res.status(200).send(seance.messages.toObject());
        } else {
            return res.status(404).send({ message: messages.Errors.NOT_FOUND });
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};
