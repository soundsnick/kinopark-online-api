import { Request, Response } from "express";
import {build, model} from "../model";
import { messages } from "../../../core";
import {checkObjectId} from "../../../core/database";
import { model as seanceModel } from "../../seance/model";
import { model as userModel } from "../../user/model";
import {authenticate} from "../../session/services";
import {Socket} from "socket.io-client";

export const send = (socket: Socket) => async (req: Request, res: Response) => {
    const input = req.body;
    const params = req.params;
    const user = await authenticate(req);
    if (user && checkObjectId(params.id)) {
        // Create user
        const seance = await seanceModel.findById(params.id);
        if (seance) {
            const message = build({ seance: seance._id, datetime: input.datetime, content: input.content, user: user._id });
            message.save(async (err) => {
                if (err) {
                    return res.status(500).send({ err });
                }
                await seanceModel.findByIdAndUpdate(seance._id, { $addToSet: { messages: message._id } });
                await userModel.findByIdAndUpdate(user._id, { $addToSet: { messages: message._id } });
                const newMessage = await model.findById(message._id).populate('user');
                socket.emit('ON_MESSAGE', { seanceId: seance._id, message: newMessage });
                return res.status(200).send(message.toObject());
            });
        } else {
            return res.status(404).send({ message: messages.Errors.NOT_FOUND });
        }
    } else {
        return res.status(400).send({
            message: messages.Errors.WRONG_INPUT
        });
    }
};
