import { Express } from "express";
import { handlers } from ".";
import { generateRoute } from "../../core/router";
import {Socket} from "socket.io-client";

export const routes = (app: Express, socket: Socket) => {
    app.post(generateRoute('/message/send/:id'), handlers.send(socket)); // id is seanceId
    app.get(generateRoute('/message/list/:id'), handlers.list); // id is seanceId
}
