import mongoose from "mongoose";

const schema = new mongoose.Schema({
    datetime: {
        type: mongoose.SchemaTypes.Date,
        required: true,
    },
    content: {
        type: String,
        required: true
    },
    seance: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Seance"
    },
    user: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "User",
    }
});

export const model = mongoose.model('Message', schema);

