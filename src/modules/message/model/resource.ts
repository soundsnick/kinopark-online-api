import { Resource as UserResource} from "../../user/model";
import { Resource as SeanceResource } from "../../seance/model";

export type Resource = {
    readonly datetime: Date;
    readonly content: string;
    readonly seance: SeanceResource;
    readonly user: UserResource;
}
