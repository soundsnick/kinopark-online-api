import mongoose from "mongoose";
import { Socket } from "socket.io-client";

export type Config = {
    databaseConnection?: Promise<typeof mongoose>,
    silent?: boolean,
    socket?: Socket
}
