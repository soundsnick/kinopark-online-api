import express from "express";
import { json, urlencoded } from "body-parser";
import { user, film, seance, message } from "../modules";
import { logger } from "../core/config";
import winston from "winston";
import cors from "cors";
import { Config } from "./type";
import { pipeline, statusLog } from "../core/utils";

export const server = async ({ databaseConnection, socket, silent }: Config) => {
    // port is now available to the Node.js runtime
    // as if it were an environment variable
    const app = express();

    app.use(json());
    app.use(cors());

    // Routes
    user.routes(app);
    seance.routes(app);
    film.routes(app);
    message.routes(app, socket);

    if (process.env.NODE_ENV !== 'production') {
        logger.add(new winston.transports.Console({
            format: winston.format.simple(),
        }));
    }

    if (!silent) {
        await pipeline(
            // Check database connection
            (next) => {
                databaseConnection
                    .then(() => {
                        statusLog('success', 'Database successfully connected');
                    })
                    .catch(() => {
                        statusLog('error', 'Database error! Check if your MongoDB server is running');
                        process.exit(0);
                    })
                    .finally(() => next(null));
            }
        )
            .then(() => void 0);
    }

    return app;
}


