import { Config } from "./type";

export * from './server';

export type ServerConfig = Config;