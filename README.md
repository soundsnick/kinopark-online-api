# OnBoarding API
Typescript, Express and MongoDB API application for OnBoarding backend.

## Requirements
- MongoDB
- Yarn
- Nodejs

## Getting started
1. Clone this repository
2. Get into project folder. `$ cd api`
3. Install dependencies `$ yarn`
4. Run development server `$ yarn dev:start`

### Scripts
All scripts are provided in `package.json` file.

### Licence
MIT
